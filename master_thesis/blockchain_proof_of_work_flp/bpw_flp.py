from typing import List, Optional, TypedDict, Union
import copy
import logging
import math
import multiprocessing
import multiprocessing.queues
import multiprocessing.synchronize
import os
import random
import signal

import psutil
from pydantic import BaseModel, BaseSettings

from master_thesis.models.u_flp_d import UFLPDInputData


class FLPSolution(BaseModel):
    customer_allocation: List[int] = []
    facility_open: List[int] = []
    total_cost: float = 0


class BPWFLPSettings(BaseSettings):
    max_iter: int = 500
    max_iter_mining: int = 200
    num_of_transactions_per_block: int = 20
    early_stopping_num_blocks: int = 20
    early_stopping_improvement: float = 0.00001
    max_invalid_blocks_in_row: int = 50
    num_halvings: int = 10
    halving_coef: float = 0.5
    multi_node: bool = False
    num_of_nodes: int = 5


class TransactionChF(TypedDict):
    name: str
    customer: int
    facility: int


class TransactionSF(TypedDict):
    name: str
    facility_old: int
    facility_new: int


class Block:
    def __init__(
        self,
        previous_value: float,
        transaction: List,
        old_solution: FLPSolution,
        index: float,
        problem_definition: UFLPDInputData,
    ) -> None:
        self.previous_value = previous_value
        self.transaction = transaction
        self.old_solution = old_solution
        self.new_solution: FLPSolution
        self.problem_data = problem_definition
        self.new_value: float
        self.transaction_combo: List
        self.index = index

    def __str__(self) -> str:
        return f"Old value is: {self.previous_value}, new value is: {self.new_value}, improvement: {self.previous_value/self.new_value} index = {self.index}."

    def compute_new_value(self) -> float:
        facilities_costs = 0
        transporations_costs = 0
        for i in range(self.problem_data.number_of_facilities):
            if self.new_solution.facility_open[i] > 0:
                facilities_costs += self.problem_data.fix_costs[i]

        for i in range(self.problem_data.number_of_customers):
            transporations_costs += self.problem_data.distance_matrix[
                self.new_solution.customer_allocation[i]
            ][i + self.problem_data.number_of_facilities]

        return facilities_costs + transporations_costs


class BlockchainFLP:
    def __init__(self, problem_definition: str, settings: Optional[BPWFLPSettings] = None):
        self.unconfirmed_transaction: List = []
        self.chain: List = []
        self.problem_data = UFLPDInputData.parse_raw(problem_definition)
        if settings:
            self.settings = settings
        else:
            self.settings = BPWFLPSettings()
        self.invalid_blocks_in_row: int = 0
        self.create_genesis_block()
        logging.basicConfig(level=logging.DEBUG)

    def create_genesis_block(self) -> None:
        init_solution = self.create_init_solution()
        genesis_block = Block(
            previous_value=999999,
            transaction=[],
            old_solution=FLPSolution(),
            index=0,
            problem_definition=self.problem_data,
        )
        genesis_block.new_value = init_solution.total_cost
        genesis_block.new_solution = init_solution
        genesis_block.transaction_combo = []
        self.chain.append(genesis_block)

    def create_init_solution(self) -> FLPSolution:
        logging.info("Creating init solution.")
        facility_open = [0] * self.problem_data.number_of_facilities
        total_costs = 0
        customer_allocation = []
        for i in range(self.problem_data.number_of_customers):
            choose_facility = random.randint(0, self.problem_data.number_of_facilities - 1)
            if facility_open[choose_facility] == 0:
                total_costs += self.problem_data.fix_costs[choose_facility]
            facility_open[choose_facility] += 1
            total_costs += self.problem_data.distance_matrix[choose_facility][
                self.problem_data.number_of_facilities + i
            ]

            customer_allocation.append(choose_facility)
        logging.info("Init solution created.")
        return FLPSolution(
            customer_allocation=customer_allocation, facility_open=facility_open, total_cost=total_costs
        )

    @property
    def last_block(self) -> Block:
        return self.chain[-1]

    def proof_of_work(self, block_to_mine: Block) -> Block:
        block = copy.deepcopy(block_to_mine)
        for i in range(self.settings.max_iter_mining):
            logging.info(f"Reshufling {i}/{self.settings.max_iter_mining}.")
            transactions = random.sample(block.transaction, random.randint(1, len(block.transaction) - 1))
            logging.info(f"Reshufled transactions {transactions}.")
            self.apply_transactions(block, transactions)
            new_value = block.compute_new_value()
            logging.info(f"New values is: {new_value} ")
            block.transaction_combo = transactions
            if new_value < block.previous_value:
                block.new_value = new_value
                logging.info("Block confirmed!")
                return block
        else:
            logging.info("Invalid block!")
            block.new_value = block.previous_value
            return block

    def proof_of_work_multi(
        self,
        result_queue: multiprocessing.queues.Queue,
        block_to_mine: Block,
        worker: int,
        main_process: str,
        foundit: multiprocessing.synchronize.Event,
    ) -> None:
        imporvement = False
        i = 0
        block = copy.deepcopy(block_to_mine)
        logging.info(f"Worker {worker} starts.")
        while i < self.settings.max_iter_mining:
            i += 1
            logging.info(f"Reshufling {i}/{self.settings.max_iter_mining}.")
            transactions = random.sample(block.transaction, random.randint(1, len(block.transaction) - 1))
            logging.info(f"Reshufled transactions {transactions}.")
            self.apply_transactions(block, transactions)
            new_value = block.compute_new_value()
            logging.info(f"New values is: {new_value} ")
            block.transaction_combo = transactions
            if new_value < block.previous_value:
                block.new_value = new_value
                imporvement = True
                logging.warning(f"Block confirmed by worker {worker}!")
                result_queue.put(block)
                foundit.set()
                break
        if not imporvement:
            logging.info("Invalid block!")
            block.new_value = block.previous_value
            result_queue.put(block)
            foundit.set()
        logging.warning(f"Worker {worker} ends.")

    def apply_transactions(self, block: Block, transactions: List) -> None:
        logging.info("Applying transactions.")
        block.new_solution = copy.deepcopy(block.old_solution)
        for transaction in transactions:
            if transaction["name"] == "ChF":
                allocated_facility_before = block.new_solution.customer_allocation[transaction["customer"]]
                if allocated_facility_before != transaction["facility"]:
                    if block.new_solution.facility_open[allocated_facility_before] > 0:
                        block.new_solution.facility_open[allocated_facility_before] += -1
                        block.new_solution.facility_open[transaction["facility"]] += 1
                        block.new_solution.customer_allocation[transaction["customer"]] = transaction[
                            "facility"
                        ]
            if transaction["name"] == "SF":
                if transaction["facility_old"] != transaction["facility_new"]:
                    if block.new_solution.facility_open[transaction["facility_old"]] > 0:
                        block.new_solution.facility_open[
                            transaction["facility_new"]
                        ] += block.new_solution.facility_open[transaction["facility_old"]]
                        block.new_solution.facility_open[transaction["facility_old"]] = 0
                        for c in range(len(block.new_solution.customer_allocation)):
                            if block.new_solution.customer_allocation[c] == transaction["facility_old"]:
                                block.new_solution.customer_allocation[c] = transaction["facility_new"]
        logging.info("Transactions applied.")

    def add_block(self, block: Block, proof: float) -> bool:
        previous_value = self.last_block.new_value
        if previous_value != block.previous_value:
            logging.warning("Invalid block. Previous values unmatch.")
            self.invalid_blocks_in_row += 1
            return False
        if not self.is_valid_proof(block, proof):
            logging.warning("Invalid block. No improvement!")
            self.invalid_blocks_in_row += 1
            return False
        block.new_value = proof
        self.chain.append(block)
        self.invalid_blocks_in_row = 0
        logging.warning(f"New block added! Improvement: {block.previous_value/block.new_value} %! ")
        return True

    def is_valid_proof(self, block: Block, new_value: float) -> bool:
        return block.previous_value > new_value

    def mine(self) -> Union[bool, float]:
        if not self.unconfirmed_transaction:
            return False

        last_block = self.last_block
        new_block = Block(
            index=last_block.index + 1,
            transaction=self.unconfirmed_transaction,
            previous_value=last_block.new_value,
            old_solution=last_block.new_solution,
            problem_definition=self.problem_data,
        )

        if self.settings.multi_node:

            result_queuq: multiprocessing.queues.Queue = multiprocessing.Queue()
            main_process = os.getpid()
            logging.info(f"Main process: {main_process}")
            # for i in range(self.settings.num_of_nodes):
            foundit = multiprocessing.Event()
            for i in range(self.settings.num_of_nodes):
                multiprocessing.Process(
                    target=self.proof_of_work_multi, args=(result_queuq, new_block, i, main_process, foundit)
                ).start()

            foundit.wait()
            new_block = result_queuq.get()
            main_process = os.getpid()
            parent = psutil.Process(main_process)
            children = parent.children(recursive=True)
            for process in children:
                logging.info(f"Process: {main_process},   killed process: {process.pid}")
                process.send_signal(signal.SIGTERM)

        else:
            new_block = self.proof_of_work(new_block)

        proof = new_block.new_value
        self.add_block(new_block, proof)
        self.unconfirmed_transaction = []
        return new_block.index

    @staticmethod
    def create_list_of_transactions(
        solution: FLPSolution, number_of_transactions: int = 3
    ) -> List[Union[TransactionChF, TransactionSF]]:
        transactions: List[Union[TransactionChF, TransactionSF]] = []
        for _ in range(number_of_transactions):
            if random.randint(0, 10) > 1:
                transactions.append(BlockchainFLP.get_change_facility(solution))
            else:
                transactions.append(BlockchainFLP.get_swap_facility(solution))

        return transactions

    @staticmethod
    def get_change_facility(solution: FLPSolution) -> TransactionChF:

        name = "ChF"
        customer = random.randint(0, len(solution.customer_allocation) - 1)
        facility = random.randint(0, len(solution.facility_open) - 1)

        return TransactionChF(name=name, customer=customer, facility=facility)

    @staticmethod
    def get_swap_facility(solution: FLPSolution) -> TransactionSF:
        name = "SF"
        facility_old = random.randint(0, len(solution.facility_open) - 1)
        facility_new = random.randint(0, len(solution.facility_open) - 1)

        return TransactionSF(name=name, facility_old=facility_old, facility_new=facility_new)

    def solve(self) -> None:
        logging.info("Start solving...")
        count_halvings = 1
        for i in range(self.settings.max_iter):
            if i == math.ceil(self.settings.max_iter / self.settings.num_halvings) * count_halvings:
                logging.warning(f"Halving {count_halvings}/{self.settings.num_halvings}")
                count_halvings += 1
                self.settings.num_of_transactions_per_block = max(
                    math.ceil(self.settings.num_of_transactions_per_block * self.settings.halving_coef), 2
                )
                self.settings.max_iter_mining = max(
                    math.ceil(self.settings.max_iter_mining * self.settings.halving_coef), 2
                )
            logging.warning(f"Iteration {i}/{self.settings.max_iter}")
            self.unconfirmed_transaction = self.create_list_of_transactions(
                self.last_block.new_solution, self.settings.num_of_transactions_per_block
            )
            self.mine()
            logging.warning(f"Actual best value: {self.chain[-1].new_value}")
            if self.early_stopping_hit():
                logging.warning("Early stopping treshold hit!")
                break
            if self.invalid_blocks_in_row >= self.settings.max_invalid_blocks_in_row:
                logging.warning("Number of invalid block in row treshold hit!")
                break

    def early_stopping_hit(self) -> bool:

        if len(self.chain) < self.settings.early_stopping_num_blocks:
            return False
        if (
            self.chain[-self.settings.early_stopping_num_blocks].new_value / self.chain[-1].new_value
            < 1 + self.settings.early_stopping_improvement
        ):
            return True
        else:
            return False


# if __name__ == "__main__":
#     input_data_flp = {
#         "number_of_facilities": 5,
#         "number_of_customers": 150,
#         "distance_matrix": utils.get_random_distance_matrix(155).values.tolist(),
#         "fix_costs": np.array(np.random.rand(5)).tolist(),
#     }
#     test = BlockchainFLP(json.dumps(input_data_flp))
#     test.solve()
#     print(test.chain)
#     # for _ in range(10):
#     #     test.unconfirmed_transaction = test.create_list_of_transactions(test.last_block.new_solution, 5)
#     #     test.mine()
#     print(test.chain)
#     # test1 = SimulatedAnnealingFLP(json.dumps(input_data_flp))
#     # test1.solve()
#     # print("ahoj")
