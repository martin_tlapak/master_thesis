from typing import List, Optional, TypedDict, Union
import copy
import json
import logging
import math
import multiprocessing
import multiprocessing.queues
import multiprocessing.synchronize
import os
import random
import signal

import numpy as np
import psutil
from pydantic import BaseModel, BaseSettings

from master_thesis import utils
from master_thesis.models.lrp_2ench_sd import LRP2ESDInputData


class LRPSD2ESolution(BaseModel):
    routes: List[List[int]] = []
    facility_open: List[int] = []
    landfill_open: List[int] = []
    total_costs: float = 0


class BPWLRPSD2ESettings(BaseSettings):
    max_iter: int = 500
    max_iter_mining: int = 200
    num_of_transactions_per_block: int = 20
    early_stopping_num_blocks: int = 20
    early_stopping_improvement: float = 0.00001
    max_invalid_blocks_in_row: int = 50
    num_halvings: int = 10
    halving_coef: float = 0.5
    multi_node: bool = False
    num_of_nodes: int = 5


class TransactionMutation(TypedDict):
    name: str
    route_delete: int
    customer_delete: int
    route_add: int
    position_add: int


class TransactionCrossover(TypedDict):
    name: str
    route_first: int
    customer_first: int
    route_second: int
    customer_second: int


class TransactionChangeFacility(TypedDict):
    name: str
    route: int
    new_facility: int


class TransactionChangeLandfill(TypedDict):
    name: str
    route: int
    new_landfill: int


class TransactionChangeOrder(TypedDict):
    name: str
    route: int


class Block:
    def __init__(
        self,
        previous_value: float,
        transaction: List,
        old_solution: LRPSD2ESolution,
        index: float,
        problem_definition: LRP2ESDInputData,
    ) -> None:
        self.previous_value = previous_value
        self.transaction = transaction
        self.old_solution = old_solution
        self.new_solution: LRPSD2ESolution
        self.problem_data = problem_definition
        self.new_value: float
        self.transaction_combo: List
        self.index = index

    def __str__(self) -> str:
        return f"Old value is: {self.previous_value}, new value is: {self.new_value}, improvement: {self.previous_value/self.new_value} index = {self.index}."

    def compute_new_value(self) -> float:
        transportation_costs = 0
        vehicle_costs = 0
        facility_costs = 0
        landfill_costs = 0

        for route in self.new_solution.routes:
            for i in range(len(route) - 1):
                transportation_costs += self.problem_data.distance_matrix[route[i]][route[i + 1]]
            vehicle_costs += self.problem_data.fix_costs_vehicles

        for index, facility in enumerate(self.new_solution.facility_open):
            if facility > 0:
                facility_costs += self.problem_data.fix_costs_facilities[index]

        for index, landfill in enumerate(self.new_solution.landfill_open):
            if landfill > 0:
                landfill_costs += self.problem_data.fix_costs_landfills[index]

        return transportation_costs + vehicle_costs + landfill_costs + facility_costs


class BlockchainLRPSD2E:
    def __init__(self, problem_definition: str, settings: Optional[BPWLRPSD2ESettings] = None):
        self.unconfirmed_transaction: List = []
        self.chain: List = []
        self.problem_data = LRP2ESDInputData.parse_raw(problem_definition)
        if settings:
            self.settings = settings
        else:
            self.settings = BPWLRPSD2ESettings()
        self.invalid_blocks_in_row: int = 0
        self.create_genesis_block()
        logging.basicConfig(level=logging.DEBUG)

    def create_genesis_block(self) -> None:
        init_solution = self.create_init_solution()
        genesis_block = Block(
            previous_value=999999,
            transaction=[],
            old_solution=LRPSD2ESolution(),
            index=0,
            problem_definition=self.problem_data,
        )
        genesis_block.new_value = init_solution.total_costs
        genesis_block.new_solution = init_solution
        genesis_block.transaction_combo = []
        self.chain.append(genesis_block)

    def create_init_solution(self) -> LRPSD2ESolution:
        logging.info("Creating init solution.")
        routes: List[List[int]] = []
        facility_open = [0] * self.problem_data.number_of_facilities
        landfill_open = [0] * self.problem_data.number_of_landfills
        total_costs = 0
        list_of_unvisited_customers = [
            customer
            for customer in range(
                self.problem_data.number_of_landfills + self.problem_data.number_of_facilities,
                self.problem_data.number_of_landfills
                + self.problem_data.number_of_facilities
                + self.problem_data.number_of_customers,
            )
        ]
        list_of_facilities = [customer for customer in range(0, self.problem_data.number_of_facilities)]
        list_of_landfills = [
            customer
            for customer in range(
                self.problem_data.number_of_facilities,
                self.problem_data.number_of_landfills + self.problem_data.number_of_facilities,
            )
        ]

        while list_of_unvisited_customers and len(routes) <= self.problem_data.number_of_vehicles:
            closest = [0, list_of_unvisited_customers[0]]
            for i in list_of_facilities:
                for j in list_of_unvisited_customers:
                    if (
                        self.problem_data.distance_matrix[i][j]
                        < self.problem_data.distance_matrix[closest[0]][closest[1]]
                    ):
                        closest = [i, j]

            route = closest
            list_of_unvisited_customers.remove(closest[1])
            facility_open[closest[0]] += 1

            if not list_of_unvisited_customers:
                closest_landfills = list_of_landfills[0]
                for l in list_of_landfills:
                    if (
                        self.problem_data.distance_matrix[route[-1]][closest_landfills]
                        > self.problem_data.distance_matrix[route[-1]][l]
                    ):
                        closest_landfills = l
                route.append(closest_landfills)
                route.append(route[0])
                landfill_open[closest_landfills - self.problem_data.number_of_facilities] += 1

            feasible = True
            while feasible and list_of_unvisited_customers:
                closest_customer = list_of_unvisited_customers[0]
                for j in list_of_unvisited_customers:
                    if (
                        self.problem_data.distance_matrix[route[-1]][closest_customer]
                        > self.problem_data.distance_matrix[route[-1]][j]
                    ):
                        closest_customer = j

                total_prob = 0
                for s in range(self.problem_data.number_of_scenarios):
                    feasible = True
                    new_route = route.copy()
                    new_route.append(closest_customer)
                    new_route.append(999)
                    new_route.append(999)
                    new_routes = routes.copy()
                    new_routes.append(new_route)
                    for r in new_routes:
                        suma = 0
                        for node in range(1, len(r) - 2):
                            suma += self.problem_data.customer_demand[
                                r[node]
                                - self.problem_data.number_of_landfills
                                - self.problem_data.number_of_facilities
                            ][s]
                        if suma > self.problem_data.vehicle_capacity:
                            feasible = False
                            break
                    if feasible:
                        total_prob += self.problem_data.probabilities[s]

                if total_prob >= self.problem_data.alpha:
                    feasible = True
                else:
                    feasible = False

                if feasible:
                    route.append(closest_customer)
                    list_of_unvisited_customers.remove(closest_customer)

                if not list_of_unvisited_customers or not feasible:
                    closest_landfills = list_of_landfills[0]
                    for l in list_of_landfills:
                        if (
                            self.problem_data.distance_matrix[route[-1]][closest_landfills]
                            > self.problem_data.distance_matrix[route[-1]][l]
                        ):
                            closest_landfills = l
                    route.append(closest_landfills)
                    route.append(route[0])
                    landfill_open[closest_landfills - self.problem_data.number_of_facilities] += 1

            routes.append(route)

        if len(routes) > self.problem_data.number_of_vehicles:
            raise ValueError("Unfeasible")

        for route in routes:
            total_costs += self.problem_data.fix_costs_vehicles
            for node in range(len(route) - 1):
                total_costs += self.problem_data.distance_matrix[route[node]][route[node + 1]]

        for index, landfill in enumerate(landfill_open):
            if landfill > 0:
                total_costs += self.problem_data.fix_costs_landfills[index]

        for index, facility in enumerate(facility_open):
            if facility > 0:
                total_costs += self.problem_data.fix_costs_facilities[index]

        logging.info("Init solution created.")
        return LRPSD2ESolution(
            routes=routes, total_costs=total_costs, facility_open=facility_open, landfill_open=landfill_open
        )

    @property
    def last_block(self) -> Block:
        return self.chain[-1]

    def check_feasibility(self, block: Block) -> bool:
        total_prob = 0
        for s in range(self.problem_data.number_of_scenarios):
            feasible = True
            for r in block.new_solution.routes:
                suma = 0
                for node in range(1, len(r) - 2):
                    suma += self.problem_data.customer_demand[
                        r[node]
                        - self.problem_data.number_of_landfills
                        - self.problem_data.number_of_facilities
                    ][s]
                if suma > self.problem_data.vehicle_capacity:
                    feasible = False
                    break
            if feasible:
                total_prob += self.problem_data.probabilities[s]

        if total_prob >= self.problem_data.alpha:
            return True
        else:
            return False

    def proof_of_work(self, block_to_mine: Block) -> Block:
        block = copy.deepcopy(block_to_mine)
        for i in range(self.settings.max_iter_mining):
            logging.info(f"Reshufling {i}/{self.settings.max_iter_mining}.")
            transactions = random.sample(block.transaction, random.randint(1, len(block.transaction) - 1))
            logging.info(f"Reshufled transactions {transactions}.")
            self.apply_transactions(block, transactions)
            if self.check_feasibility(block):
                new_value = block.compute_new_value()
            else:
                new_value = 999999
            logging.info(f"New values is: {new_value} ")
            block.transaction_combo = transactions
            if new_value < block.previous_value:
                block.new_value = new_value
                logging.info("Block confirmed!")
                return block
        else:
            logging.info("Invalid block!")
            block.new_value = block.previous_value
            return block

    def proof_of_work_multi(
        self,
        result_queue: multiprocessing.queues.Queue,
        block_to_mine: Block,
        worker: int,
        main_process: str,
        foundit: multiprocessing.synchronize.Event,
    ) -> None:
        imporvement = False
        i = 0
        block = copy.deepcopy(block_to_mine)
        logging.info(f"Worker {worker} starts.")
        while i < self.settings.max_iter_mining:
            i += 1
            logging.info(f"Reshufling {i}/{self.settings.max_iter_mining}.")
            transactions = random.sample(block.transaction, random.randint(1, len(block.transaction) - 1))
            logging.info(f"Reshufled transactions {transactions}.")
            self.apply_transactions(block, transactions)
            if self.check_feasibility(block):
                new_value = block.compute_new_value()
            else:
                new_value = 999999
            logging.info(f"New values is: {new_value} ")
            block.transaction_combo = transactions
            if new_value < block.previous_value:
                block.new_value = new_value
                imporvement = True
                logging.warning(f"Block confirmed by worker {worker}!")
                result_queue.put(block)
                foundit.set()
                break
        if not imporvement:
            logging.info("Invalid block!")
            block.new_value = block.previous_value
            result_queue.put(block)
            foundit.set()
        logging.warning(f"Worker {worker} ends.")

    def apply_transactions(self, block: Block, transactions: List) -> None:
        logging.info("Applying transactions.")
        block.new_solution = copy.deepcopy(block.old_solution)
        for transaction in transactions:
            if transaction["name"] == "mutation":
                if transaction["route_delete"] == transaction["route_add"]:
                    continue
                if transaction["route_delete"] > len(block.new_solution.routes) - 1:
                    continue
                if transaction["route_add"] > len(block.new_solution.routes) - 1:
                    continue
                if transaction["customer_delete"] + 3 > len(
                    block.new_solution.routes[transaction["route_delete"]]
                ):
                    continue
                if transaction["position_add"] + 3 > len(block.new_solution.routes[transaction["route_add"]]):
                    continue

                if (
                    transaction["customer_delete"] == 1
                    and len(block.new_solution.routes[transaction["route_delete"]]) == 4
                ):
                    block.new_solution.routes[transaction["route_add"]].insert(
                        transaction["position_add"],
                        block.new_solution.routes[transaction["route_delete"]][
                            transaction["customer_delete"]
                        ],
                    )
                    block.new_solution.routes.pop(transaction["route_delete"])
                else:
                    block.new_solution.routes[transaction["route_add"]].insert(
                        transaction["position_add"],
                        block.new_solution.routes[transaction["route_delete"]][
                            transaction["customer_delete"]
                        ],
                    )
                    block.new_solution.routes[transaction["route_delete"]].pop(transaction["customer_delete"])
            if transaction["name"] == "crossover":
                if transaction["route_first"] > len(block.new_solution.routes) - 1:
                    continue
                if transaction["route_second"] > len(block.new_solution.routes) - 1:
                    continue
                if transaction["customer_first"] + 3 > len(
                    block.new_solution.routes[transaction["route_first"]]
                ):
                    continue
                if transaction["customer_second"] + 3 > len(
                    block.new_solution.routes[transaction["route_second"]]
                ):
                    continue

                first_value = block.new_solution.routes[transaction["route_first"]][
                    transaction["customer_first"]
                ]
                second_value = block.new_solution.routes[transaction["route_second"]][
                    transaction["customer_second"]
                ]
                block.new_solution.routes[transaction["route_first"]][
                    transaction["customer_first"]
                ] = second_value
                block.new_solution.routes[transaction["route_second"]][
                    transaction["customer_second"]
                ] = first_value

            if transaction["name"] == "changefacility":
                if transaction["route"] > len(block.new_solution.routes) - 1:
                    continue
                block.new_solution.facility_open[block.new_solution.routes[transaction["route"]][0]] += -1
                block.new_solution.routes[transaction["route"]][0] = transaction["new_facility"]
                block.new_solution.routes[transaction["route"]][-1] = transaction["new_facility"]
                block.new_solution.facility_open[transaction["new_facility"]] += 1

            if transaction["name"] == "changelandfill":
                if transaction["route"] > len(block.new_solution.routes) - 1:
                    continue
                block.new_solution.landfill_open[
                    block.new_solution.routes[transaction["route"]][-2]
                    - self.problem_data.number_of_facilities
                ] += -1
                block.new_solution.routes[transaction["route"]][-2] = transaction["new_landfill"]
                block.new_solution.landfill_open[
                    transaction["new_landfill"] - self.problem_data.number_of_facilities
                ] += 1

            # if transaction["name"] == "changeorder":
            #     if transaction["route"] > len(block.new_solution.routes) - 1:
            #         continue
            #     customer_subroute = block.new_solution.routes[transaction["route"]][1:-2]
            #     reorder_customer_subroute = customer_subroute[::-1]
            #     block.new_solution.routes[transaction["route"]][1:-2] = reorder_customer_subroute

        logging.info("Transactions applied.")

    def add_block(self, block: Block, proof: float) -> bool:
        previous_value = self.last_block.new_value
        if previous_value != block.previous_value:
            logging.warning("Invalid block. Previous values unmatch.")
            self.invalid_blocks_in_row += 1
            return False
        if not self.is_valid_proof(block, proof):
            logging.warning("Invalid block. No improvement!")
            self.invalid_blocks_in_row += 1
            return False
        block.new_value = proof
        self.chain.append(block)
        self.invalid_blocks_in_row = 0
        logging.warning(f"New block added! Improvement: {block.previous_value/block.new_value} %! ")
        return True

    def is_valid_proof(self, block: Block, new_value: float) -> bool:
        return block.previous_value > new_value

    def mine(self) -> Union[bool, float]:
        if not self.unconfirmed_transaction:
            return False

        last_block = self.last_block
        new_block = Block(
            index=last_block.index + 1,
            transaction=self.unconfirmed_transaction,
            previous_value=last_block.new_value,
            old_solution=last_block.new_solution,
            problem_definition=self.problem_data,
        )

        if self.settings.multi_node:

            result_queuq: multiprocessing.queues.Queue = multiprocessing.Queue()
            main_process = os.getpid()
            logging.info(f"Main process: {main_process}")
            # for i in range(self.settings.num_of_nodes):
            foundit = multiprocessing.Event()
            for i in range(self.settings.num_of_nodes):
                multiprocessing.Process(
                    target=self.proof_of_work_multi, args=(result_queuq, new_block, i, main_process, foundit)
                ).start()

            foundit.wait()
            new_block = result_queuq.get()
            main_process = os.getpid()
            parent = psutil.Process(main_process)
            children = parent.children(recursive=True)
            for process in children:
                logging.info(f"Process: {main_process},   killed process: {process.pid}")
                process.send_signal(signal.SIGTERM)

        else:
            new_block = self.proof_of_work(new_block)

        proof = new_block.new_value
        self.add_block(new_block, proof)
        self.unconfirmed_transaction = []
        return new_block.index

    def create_list_of_transactions(
        self, solution: LRPSD2ESolution, number_of_transactions: int = 3
    ) -> List[
        Union[
            TransactionMutation,
            TransactionCrossover,
            TransactionChangeFacility,
            TransactionChangeLandfill,
            TransactionChangeOrder,
        ]
    ]:
        transactions: List[
            Union[
                TransactionMutation,
                TransactionCrossover,
                TransactionChangeFacility,
                TransactionChangeLandfill,
                TransactionChangeOrder,
            ]
        ] = []
        for _ in range(number_of_transactions):
            roll_dice = random.randint(0, 10)
            if roll_dice in range(0, 4):
                transactions.append(BlockchainLRPSD2E.get_change_mutation(solution))
            elif roll_dice in range(4, 9):
                transactions.append(BlockchainLRPSD2E.get_change_crossover(solution))
            elif roll_dice in range(9, 10):
                transactions.append(self.get_cahnge_facility(solution))
            # elif roll_dice in range(10,11):
            #     transactions.append(self.get_cahnge_order(solution))
            else:
                transactions.append(self.get_cahnge_landfill(solution))
        return transactions

    @staticmethod
    def get_change_mutation(solution: LRPSD2ESolution) -> TransactionMutation:

        name = "mutation"
        route_delete = random.randint(0, len(solution.routes) - 1)
        customer_delete = random.randint(1, len(solution.routes[route_delete]) - 3)
        route_add = random.randint(0, len(solution.routes) - 1)
        position_add = random.randint(1, len(solution.routes[route_add]) - 3)

        return TransactionMutation(
            name=name,
            route_delete=route_delete,
            customer_delete=customer_delete,
            route_add=route_add,
            position_add=position_add,
        )

    @staticmethod
    def get_change_crossover(solution: LRPSD2ESolution) -> TransactionCrossover:

        name = "crossover"
        route_first = random.randint(0, len(solution.routes) - 1)
        route_second = random.randint(0, len(solution.routes) - 1)
        customer_first = random.randint(1, len(solution.routes[route_first]) - 3)
        customer_second = random.randint(1, len(solution.routes[route_second]) - 3)

        return TransactionCrossover(
            name=name,
            route_first=route_first,
            route_second=route_second,
            customer_first=customer_first,
            customer_second=customer_second,
        )

    def get_cahnge_facility(self, solution: LRPSD2ESolution) -> TransactionChangeFacility:
        name = "changefacility"
        route = random.randint(0, len(solution.routes) - 1)
        new_facility = random.randint(0, self.problem_data.number_of_facilities - 1)

        return TransactionChangeFacility(name=name, route=route, new_facility=new_facility)

    def get_cahnge_landfill(self, solution: LRPSD2ESolution) -> TransactionChangeLandfill:
        name = "changelandfill"
        route = random.randint(0, len(solution.routes) - 1)
        new_landfill = random.randint(
            0 + self.problem_data.number_of_facilities,
            self.problem_data.number_of_landfills + self.problem_data.number_of_facilities - 1,
        )

        return TransactionChangeLandfill(name=name, route=route, new_landfill=new_landfill)

    def get_cahnge_order(self, solution: LRPSD2ESolution) -> TransactionChangeOrder:
        name = "changeorder"
        route = random.randint(0, len(solution.routes) - 1)

        return TransactionChangeOrder(name=name, route=route)

    def solve(self) -> None:
        logging.info("Start solving...")
        count_halvings = 1
        for i in range(self.settings.max_iter):
            if i == math.ceil(self.settings.max_iter / self.settings.num_halvings) * count_halvings:
                logging.warning(f"Halving {count_halvings}/{self.settings.num_halvings}")
                count_halvings += 1
                self.settings.num_of_transactions_per_block = max(
                    math.ceil(self.settings.num_of_transactions_per_block * self.settings.halving_coef), 2
                )
                self.settings.max_iter_mining = max(
                    math.ceil(self.settings.max_iter_mining * self.settings.halving_coef), 2
                )
            logging.warning(f"Iteration {i}/{self.settings.max_iter}")
            self.unconfirmed_transaction = self.create_list_of_transactions(
                self.last_block.new_solution, self.settings.num_of_transactions_per_block
            )
            self.mine()
            logging.warning(f"Actual best value: {self.chain[-1].new_value}")
            if self.early_stopping_hit():
                logging.warning("Early stopping treshold hit!")
                break
            if self.invalid_blocks_in_row >= self.settings.max_invalid_blocks_in_row:
                logging.warning("Number of invalid block in row treshold hit!")
                break

    def early_stopping_hit(self) -> bool:

        if len(self.chain) < self.settings.early_stopping_num_blocks:
            return False
        if (
            self.chain[-self.settings.early_stopping_num_blocks].new_value / self.chain[-1].new_value
            < 1 + self.settings.early_stopping_improvement
        ):
            return True
        else:
            return False


if __name__ == "__main__":
    number_of_facilities = 3
    number_of_customers = 5
    number_of_landfills = 1
    number_of_vehicles = 2
    vehicle_capacity = 2
    number_of_scenarios = 40
    customer_demand = np.random.rand(number_of_customers, number_of_scenarios)
    alpha = 0.8
    probabilities = [1 / number_of_scenarios] * number_of_scenarios
    fix_costs_vehicles = 0.01

    distance_matrix, coordinates = utils.get_random_distance_matrix_coordinates(
        number_of_facilities + number_of_customers + number_of_landfills
    )

    input_data_vrpsd2e = {
        "number_of_facilities": number_of_facilities,
        "number_of_customers": number_of_customers,
        "number_of_landfills": number_of_landfills,
        "distance_matrix": distance_matrix.values.tolist(),
        "vehicle_capacity": vehicle_capacity,
        "customer_demand": customer_demand.tolist(),
        "number_of_scenarios": number_of_scenarios,
        "alpha": alpha,
        "probabilities": probabilities,
        "fix_costs_vehicles": fix_costs_vehicles,
        "number_of_vehicles": number_of_vehicles,
        "fix_costs_facilities": np.random.rand(number_of_facilities).tolist(),
        "fix_costs_landfills": np.random.rand(number_of_landfills).tolist(),
    }
    settings_bch = BPWLRPSD2ESettings(
        max_iter=20000,
        max_iter_mining=400,
        early_stopping_num_blocks=200,
        num_of_transactions_per_block=10,
        early_stopping_improvement=0.001,
        max_invalid_blocks_in_row=200,
        num_halvings=40,
        halving_coef=0.5,
        multi_node=False,
        num_of_nodes=10,
    )
    blockchain = BlockchainLRPSD2E(json.dumps(input_data_vrpsd2e), settings_bch)
    blockchain.solve()
    # blockchain.solve()
    print("test")
