from typing import List
import json
import logging
import random

import numpy as np
import pandas as pd
from pydantic import BaseModel

from master_thesis import utils
from master_thesis.models.lrp_d import LRPD
from master_thesis.models.u_flp_d import UFLPD
from master_thesis.models.vrp_d import VRPD


class InputLRPvsFLPVRP(BaseModel):
    number_of_facilities: int = 3
    number_of_customers: int = 8
    number_of_vehicles: int = 3
    number_of_simulations: int = 10
    simulation_scenario: str = "base"
    fix_costs_vehicles: float = 1
    vehicle_capacity: float = 5


class LRPvsFLPVRPDeterministic:
    HIGH_TRANSPORT_COSTS = 10
    HIGH_FACILITY_COSTS = 10
    SEED = 1994

    def __init__(self, data: str) -> None:
        self.input_data = InputLRPvsFLPVRP.parse_raw(data)
        self.simulated_data: List[dict] = list()
        self.results: dict = dict()
        logging.basicConfig(level=logging.DEBUG)

    def simulate_data(self) -> None:
        random.seed(self.SEED)
        logging.info("Simulation of data starts.")
        for _ in range(self.input_data.number_of_simulations):
            simulation = dict()
            if self.input_data.simulation_scenario == "base":
                simulation["distance_matrix"] = utils.get_random_distance_matrix(
                    self.input_data.number_of_facilities + self.input_data.number_of_customers
                )
                simulation["facility_costs"] = np.random.rand(self.input_data.number_of_facilities)
                simulation["customer_demand"] = np.random.rand(self.input_data.number_of_customers)
            elif self.input_data.simulation_scenario == "high_transport_costs":
                simulation["distance_matrix"] = (
                    utils.get_random_distance_matrix(
                        self.input_data.number_of_facilities + self.input_data.number_of_customers
                    )
                    * self.HIGH_TRANSPORT_COSTS
                )
                simulation["facility_costs"] = np.random.rand(self.input_data.number_of_facilities)
                simulation["customer_demand"] = np.random.rand(self.input_data.number_of_customers)
            elif self.input_data.simulation_scenario == "high_facility_costs":
                simulation["distance_matrix"] = utils.get_random_distance_matrix(
                    self.input_data.number_of_facilities + self.input_data.number_of_customers
                )
                simulation["facility_costs"] = (
                    np.random.rand(self.input_data.number_of_facilities) * self.HIGH_FACILITY_COSTS
                )
                simulation["customer_demand"] = np.random.rand(self.input_data.number_of_customers)
            else:
                raise ValueError("Unknown scenario")

            self.simulated_data.append(simulation)
        logging.info("Simulation of data ends.")

    def run_simulations(self) -> None:
        if not self.simulated_data:
            self.simulate_data()

        for index, simulation in enumerate(self.simulated_data):
            index_of_iteration = f"{index+1}/{self.input_data.number_of_simulations}"
            logging.info(f"Computation for simulation {index_of_iteration} starts")

            # FLP
            logging.info(f"Creating input for FLP ({index_of_iteration})")
            input_data_flp = {
                "number_of_facilities": self.input_data.number_of_facilities,
                "number_of_customers": self.input_data.number_of_customers,
                "distance_matrix": simulation["distance_matrix"].values.tolist(),
                "fix_costs": simulation["facility_costs"].tolist(),
            }
            logging.info(f"Solving FLP ({index_of_iteration})")
            flp = UFLPD(json.dumps(input_data_flp))
            flp.compute()
            logging.info(f"Total costs of FLP: {flp.model.objective_value} ({index_of_iteration})")

            # facility costs
            facility_costs = np.array(simulation["facility_costs"])[
                [
                    j
                    for j in range(self.input_data.number_of_facilities)
                    if flp.model.var_by_name(f"facility[{j}]").x > 0.99
                ]
            ].sum()

            # VRP
            logging.info(f"Drop unused indecies for VRP ({index_of_iteration})")
            drop_index = [
                j
                for j in range(self.input_data.number_of_facilities)
                if flp.model.var_by_name(f"facility[{j}]").x < 0.99
            ]
            distance_matrix_vrp = simulation["distance_matrix"].drop(drop_index, axis=0)
            distance_matrix_vrp = distance_matrix_vrp.drop(drop_index, axis=1)
            # number of facilities for VRP
            number_of_facilities_VRP = self.input_data.number_of_facilities - len(drop_index)

            logging.info(f"Creating input for VRP ({index_of_iteration})")
            input_data_vrpsd = {
                "vehicle_capacity": self.input_data.vehicle_capacity,
                "fix_costs": self.input_data.fix_costs_vehicles,
                "number_of_vehicles": self.input_data.number_of_vehicles,
                "number_of_facilities": number_of_facilities_VRP,
                "number_of_customers": self.input_data.number_of_customers,
                "distance_matrix": distance_matrix_vrp.values.tolist(),
                "customer_demand": simulation["customer_demand"].tolist(),
            }
            logging.info(f"Solving VRP ({index_of_iteration})")
            vrp = VRPD(json.dumps(input_data_vrpsd))
            vrp.compute()
            logging.info(f"Total costs of VRP: {flp.model.objective_value} ({index_of_iteration})")

            # LRP
            logging.info(f"Creating input for LRP ({index_of_iteration})")
            input_data_lrpd = {
                "vehicle_capacity": self.input_data.vehicle_capacity,
                "fix_costs_vehicles": self.input_data.fix_costs_vehicles,
                "fix_costs_facilities": simulation["facility_costs"].tolist(),
                "number_of_vehicles": self.input_data.number_of_vehicles,
                "number_of_facilities": self.input_data.number_of_facilities,
                "number_of_customers": self.input_data.number_of_customers,
                "distance_matrix": simulation["distance_matrix"].values.tolist(),
                "customer_demand": simulation["customer_demand"].tolist(),
            }
            logging.info(f"Solving LRP ({index_of_iteration})")
            lrp = LRPD(json.dumps(input_data_lrpd))
            lrp.compute()
            logging.info(f"Total costs of LRP: {lrp.model.objective_value} ({index_of_iteration})")

            # results
            results = [
                round(facility_costs + vrp.model.objective_value, 2),
                round(lrp.model.objective_value, 2),
                round(1 - lrp.model.objective_value / (facility_costs + vrp.model.objective_value), 3) * 100,
            ]
            self.results[index] = results

    def results_to_df(self) -> pd.DataFrame:
        if self.results:
            return pd.DataFrame.from_dict(self.results, orient="index", columns=["FLP+VRP", "LRP", "Poměr"])
        else:
            return pd.DataFrame()
