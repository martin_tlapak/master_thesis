from typing import List
import json
import math
import random

import numpy as np

from master_thesis import utils
from master_thesis.models.u_flp_d import UFLPDInputData


LOOP_LENGTH = 1000
MAX_TEMPERATURE = 100
MIN_TEMPERATURE = 0.5
ATTENUATION_QUOTIENT = 0.99


class SimulatedAnnealingFLP:
    def __init__(self, problem_definition: str) -> None:
        self.problem_data = UFLPDInputData.parse_raw(problem_definition)
        self.facility_open = [0] * self.problem_data.number_of_facilities
        self.customer_allocation: List[int] = []
        self.total_cost: float = 0
        self.temperature: float = MAX_TEMPERATURE

    def init_alloc_customer(self) -> None:
        for i in range(self.problem_data.number_of_customers):
            choose_facility = random.randint(0, self.problem_data.number_of_facilities - 1)
            if self.facility_open[choose_facility] == 0:
                self.total_cost += self.problem_data.fix_costs[choose_facility]
            self.facility_open[choose_facility] += 1
            self.total_cost += self.problem_data.distance_matrix[choose_facility][
                self.problem_data.number_of_facilities + i
            ]

            self.customer_allocation.append(choose_facility)

    def neighbor_change_facility(self) -> bool:
        while True:
            choose_customer = random.randint(0, self.problem_data.number_of_customers - 1)
            choose_facility = random.randint(0, self.problem_data.number_of_facilities - 1)
            if self.customer_allocation[choose_customer] == choose_facility:
                continue
            allocated_facility_before = self.customer_allocation[choose_customer]
            cost_before = self.problem_data.distance_matrix[allocated_facility_before][
                choose_customer + self.problem_data.number_of_facilities
            ]
            if self.facility_open[allocated_facility_before] == 1:
                cost_before += self.problem_data.fix_costs[allocated_facility_before]
            cost_after = self.problem_data.distance_matrix[choose_facility][
                choose_customer + self.problem_data.number_of_facilities
            ]
            if self.facility_open[choose_facility] == 0:
                cost_after += self.problem_data.fix_costs[choose_facility]

            if cost_before <= cost_after and random.random() > math.exp(
                (cost_before - cost_after) / self.temperature
            ):
                return False

            self.facility_open[allocated_facility_before] -= 1
            self.facility_open[choose_facility] += 1

            self.customer_allocation[choose_customer] = choose_facility
            self.total_cost = self.total_cost - cost_before + cost_after

            return True

    def solve(self) -> float:
        self.init_alloc_customer()
        while self.temperature > MIN_TEMPERATURE:
            for _ in range(0, LOOP_LENGTH):
                if self.neighbor_change_facility():
                    print(f"Temperature is {self.temperature} and total costs are {self.total_cost}")
                    print(f"Facilities open {self.facility_open}")
                    print(f"Customer_alocation {self.customer_allocation}")
            self.temperature *= ATTENUATION_QUOTIENT
        return self.total_cost


if __name__ == "__main__":
    input_data_flp = {
        "number_of_facilities": 5,
        "number_of_customers": 10,
        "distance_matrix": utils.get_random_distance_matrix(15).values.tolist(),
        "fix_costs": np.array(np.random.rand(5) + 10).tolist(),
    }
    test = SimulatedAnnealingFLP(json.dumps(input_data_flp))
    test.solve()
