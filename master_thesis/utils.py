from typing import Tuple

import numpy as np
import pandas as pd
from scipy.spatial import distance_matrix


def get_random_distance_matrix(dimension: int) -> pd.DataFrame:
    """Get random dintance matrix. By disntace matrix we mean zero-diagonal symmetric matrix such that the triagle inequality holds.

    Parameters:
    -----------

    dimension: int
        - Matrix dimension.

    Return:
    -------
    pd.DataFrame
    """
    x_coordinates = np.random.rand(dimension)
    y_coordinates = np.random.rand(dimension)
    data = np.column_stack((x_coordinates, y_coordinates))
    df = pd.DataFrame(data, columns=["xcord", "ycord"])
    return pd.DataFrame(distance_matrix(df.values, df.values), index=df.index, columns=df.index)


def get_random_distance_matrix_coordinates(dimension: int) -> Tuple[pd.DataFrame, np.ndarray]:
    """Get random dintance matrix. By disntace matrix we mean zero-diagonal symmetric matrix such that the triagle inequality holds.

    Parameters:
    -----------

    dimension: int
        - Matrix dimension.

    Return:
    -------
    pd.DataFrame
    """
    x_coordinates = np.random.rand(dimension)
    y_coordinates = np.random.rand(dimension)
    data = np.column_stack((x_coordinates, y_coordinates))
    df = pd.DataFrame(data, columns=["xcord", "ycord"])
    return pd.DataFrame(distance_matrix(df.values, df.values), index=df.index, columns=df.index), data
