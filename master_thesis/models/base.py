import abc


class Models(abc.ABC):
    """Base class for all models"""

    @abc.abstractmethod
    def define_model(self) -> None:
        pass

    # @abc.abstractmethod
    # def check_if_feasible(self, solution_to_check: Union[str, bytes]) -> bool:
    #     pass

    @abc.abstractmethod
    def compute(self) -> None:
        pass
