from typing import Any, List, Union

from mip import BINARY, Model, minimize, xsum
import numpy as np
from pydantic import BaseModel

from master_thesis.models.base import Models


class UFLPD2EInputData(BaseModel):
    number_of_facilities: int
    number_of_customers: int
    number_of_factories: int
    distance_matrix: List[List[float]]
    fix_costs_facilities: List[float]
    fix_costs_factories: List[float]


class UFLPD2E(Models):
    def __init__(self, input_data: Union[str, bytes]) -> None:
        self.data = UFLPD2EInputData.parse_raw(input_data)
        self.model = self.define_model()

    def define_model(self) -> Any:

        number_of_facilities = self.data.number_of_facilities
        number_of_customers = self.data.number_of_customers
        number_of_factories = self.data.number_of_factories
        distance_matrix = np.array(self.data.distance_matrix)
        fix_costs_facilities = np.array(self.data.fix_costs_facilities)
        fix_costs_factories = np.array(self.data.fix_costs_factories)

        # set of indecies for facilities
        I = set(range(number_of_facilities))

        # set of indecies for customers
        J = set(range(number_of_customers))

        # set of indecies for factories
        K = set(range(number_of_factories))

        model = Model(name="UFLRD", solver_name="CBC")

        # continuous  variable x_{ij}
        x = [[model.add_var(var_type=BINARY, name=f"routes[{i}][{j}]") for j in J] for i in I]

        # binary variable y_{i}
        y = [model.add_var(var_type=BINARY, name=f"facility[{i}]") for i in I]

        # binary  variable w_{ki}
        w = [[model.add_var(var_type=BINARY) for i in I] for k in K]

        # binary  variable z_{k}
        z = [model.add_var(var_type=BINARY) for k in K]

        # objective function (2.3)
        model.objective = minimize(
            xsum(
                distance_matrix[i + number_of_factories][j + number_of_facilities + number_of_factories]
                * x[i][j]
                for i in I
                for j in J
            )
            + xsum(fix_costs_facilities[i] * y[i] for i in I)
            + xsum(fix_costs_factories[k] * z[k] for k in K)
            + xsum(distance_matrix[k][i + number_of_factories] * w[k][i] for k in K for i in I)
        )

        # constraint (2.4)
        for j in J:
            model += xsum(x[i][j] for i in I) == 1

        # constraint (2.5)
        for i in I:
            for j in J:
                model += x[i][j] <= y[i]

        # constraint (2.4)
        for i in I:
            model += xsum(w[k][i] for k in K) == y[i]

        # constraint (2.5)
        for k in K:
            for i in I:
                model += w[k][i] <= z[k]

        return model

    def compute(self) -> None:
        self.model.max_mip_gap_abs = 0.00001
        self.model.optimize()

    def check_if_feasbile(self, solution_to_check: Union[str, bytes]) -> bool:
        pass
