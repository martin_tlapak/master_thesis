from typing import Any, List, Union
from itertools import product
import json

from mip import BINARY, Model, minimize, xsum
import numpy as np
from pydantic import BaseModel

from master_thesis import utils
from master_thesis.models.base import Models


class LRP2ESDInputData(BaseModel):
    number_of_facilities: int
    number_of_customers: int
    number_of_landfills: int
    number_of_vehicles: int
    number_of_scenarios: int
    vehicle_capacity: int
    fix_costs_vehicles: float
    fix_costs_facilities: List[float]
    fix_costs_landfills: List[float]
    distance_matrix: List[List[float]]
    customer_demand: List[List[float]]
    probabilities: List[float]
    alpha: float


class LRP2ESD(Models):
    def __init__(self, input_data: Union[str, bytes], solver: str = "GRB") -> None:
        self.data = LRP2ESDInputData.parse_raw(input_data)
        self.model = self.define_model(solver)

    def define_model(self, solver: str) -> Any:

        number_of_facilities = self.data.number_of_facilities
        number_of_customers = self.data.number_of_customers
        number_of_landfills = self.data.number_of_landfills
        number_of_vehicles = self.data.number_of_vehicles
        number_of_scenarios = self.data.number_of_scenarios
        vehicle_capacity = self.data.vehicle_capacity
        fix_costs_vehicles = self.data.fix_costs_vehicles
        fix_costs_facilities = np.array(self.data.fix_costs_facilities)
        fix_costs_landfills = np.array(self.data.fix_costs_landfills)
        distance_matrix = np.array(self.data.distance_matrix)
        customer_demand = np.array(self.data.customer_demand)
        probabilities = np.array(self.data.probabilities)
        alpha = self.data.alpha

        # set of indecies for facilities
        I = set(range(number_of_facilities))

        # set of indecies for customers
        J = set(
            range(
                number_of_facilities + number_of_landfills,
                number_of_customers + number_of_landfills + number_of_facilities,
            )
        )

        # set of indecies for landfills
        L = set(range(number_of_facilities, number_of_landfills + number_of_facilities))

        # set of all vertecies
        M = I.union(L).union(J)

        # set of facilities and customers
        N = I.union(J)

        # set of indecies for factories
        K = set(range(number_of_vehicles))

        # set of indecies for scenarios
        S = set(range(number_of_scenarios))

        model = Model(name="VRPD", solver_name=solver)

        # binary  variable x_{ijk}
        x = [
            [[model.add_var(var_type=BINARY, name=f"route[{i}][{j}][{k}]") for k in K] for j in M] for i in M
        ]

        # binary variable y_{i}
        y = [model.add_var(var_type=BINARY, name=f"facility[{i}]") for i in I]

        # binary variable w_{l}
        w = [model.add_var(var_type=BINARY, name=f"landfill[{l}]") for l in L]

        # variable v_i
        v = [model.add_var(name=f"vehicles[{i}]") for i in I]

        # variable u_{jk}
        u = [[model.add_var() for k in K] for j in J]

        # variable z_s
        z = [model.add_var(var_type=BINARY, name=f"scenario[{s}]") for s in S]

        # objective function (3.7)
        model.objective = minimize(
            xsum(distance_matrix[i][j] * x[i][j][k] for (i, j, k) in product(M, M, K))
            + xsum(fix_costs_facilities[i] * y[i] for i in I)
            + xsum(fix_costs_vehicles * v[i] for i in I)
            + xsum(fix_costs_landfills[l - number_of_facilities] * w[l - number_of_facilities] for l in L)
        )

        # constraint (3.8)
        for j in J:
            model += xsum(x[i][j][k] for (i, k) in product(N, K)) == 1

        # # # constraint (3.9)
        for (j, k) in product(M, K):
            model += xsum(x[i][j][k] for i in M) - xsum(x[j][i][k] for i in M) == 0

        # # # constraint (3.10)
        for k in K:
            model += xsum(x[i][j][k] for (i, j) in product(I, J)) <= 1

        # # # constraint (3.13)
        for (j1, j2, k) in product(J, J, K):
            model += (
                u[j1 - number_of_facilities - number_of_landfills][k]
                - u[j2 - number_of_facilities - number_of_landfills][k]
                + number_of_customers * x[j1][j2][k]
                <= number_of_customers - 1
            )

        # constraint (3.11)
        for i in I:
            model += xsum(x[i][j][k] for (j, k) in product(J, K)) - v[i] == 0

        # constraint (3.12)
        for i in I:
            model += v[i] <= number_of_vehicles * y[i]

        # # # constraint (3.14)
        for (k, s) in product(K, S):
            model += (
                xsum(
                    customer_demand[j - number_of_facilities - number_of_landfills][s] * x[i][j][k]
                    for (j, i) in product(J, N)
                )
                - vehicle_capacity
                - 100 * (1 - z[s])
                <= 0
            )

        #  # constraint (3.15)
        model += xsum(probabilities[s] * z[s] for s in S) >= alpha

        # constraint (3.16)
        for k in K:
            model += (
                xsum(x[i][j][k] for (i, j) in product(I, J)) - xsum(x[j][l][k] for (j, l) in product(J, L))
                == 0
            )

        # # constraint (3.17)
        for k in K:
            model += (
                xsum(x[l][i][k] for (l, i) in product(L, I)) - xsum(x[j][l][k] for (j, l) in product(J, L))
                == 0
            )

        model += xsum(x[i][l][k] for (i, l, k) in product(I, L, K)) == 0

        # # constraint (3.18)
        for l in L:
            model += (
                xsum(x[j][l][k] for (j, k) in product(J, K))
                <= number_of_vehicles * w[l - number_of_facilities]
            )

        # # # contraint (3.20)
        for (j, k) in product(J, K):
            model += u[j - number_of_facilities - number_of_landfills][k] >= 0

        return model

    def compute(self) -> None:
        self.model.optimize()

    def check_if_feasbile(self, solution_to_check: Union[str, bytes]) -> bool:
        pass


if __name__ == "__main__":
    input_data_lrpd = {
        "vehicle_capacity": 1000,
        "fix_costs_vehicles": 1,
        "fix_costs_facilities": np.random.rand(2).tolist(),
        "fix_costs_landfills": np.random.rand(2).tolist(),
        "number_of_vehicles": 1,
        "number_of_facilities": 2,
        "number_of_customers": 5,
        "number_of_landfills": 2,
        "distance_matrix": utils.get_random_distance_matrix(9).values.tolist(),
        "customer_demand": np.random.rand(5, 4).tolist(),
        "alpha": 0.5,
        "probabilities": [1 / 4] * 4,
        "number_of_scenarios": 4,
    }
    model = LRP2ESD(json.dumps(input_data_lrpd))
    model.compute()
    if model.model.num_solutions:
        print(f"Total costs: {model.model.objective_value}")
    else:
        print("unfeasible")
