from typing import Any, List, Union
from itertools import product

from mip import BINARY, Model, minimize, xsum
import numpy as np
from pydantic import BaseModel

from master_thesis.models.base import Models


class LRPSDInputData(BaseModel):
    number_of_facilities: int
    number_of_customers: int
    number_of_vehicles: int
    number_of_scenarios: int
    vehicle_capacity: int
    fix_costs_vehicles: int
    fix_costs_facilities: List[float]
    distance_matrix: List[List[float]]
    customer_demand: List[List[float]]
    probabilities: List[float]
    alpha: float


class LRPSD(Models):
    def __init__(self, input_data: Union[str, bytes]) -> None:
        self.data = LRPSDInputData.parse_raw(input_data)
        self.model = self.define_model()

    def define_model(self) -> Any:

        number_of_facilities = self.data.number_of_facilities
        number_of_customers = self.data.number_of_customers
        number_of_vehicles = self.data.number_of_vehicles
        number_of_scenarios = self.data.number_of_scenarios
        vehicle_capacity = self.data.vehicle_capacity
        fix_costs_vehicles = self.data.fix_costs_vehicles
        fix_costs_facilities = np.array(self.data.fix_costs_facilities)
        distance_matrix = np.array(self.data.distance_matrix)
        customer_demand = np.array(self.data.customer_demand)
        probabilities = np.array(self.data.probabilities)
        alpha = self.data.alpha

        # set of indecies for facilities
        I = set(range(number_of_facilities))

        # set of indecies for customers
        J = set(range(number_of_facilities, number_of_customers + number_of_facilities))

        # set of all vertecies
        N = I.union(J)

        # set of indecies for factories
        K = set(range(number_of_vehicles))

        # set of indecies for scenarios
        S = set(range(number_of_scenarios))

        model = Model(name="VRPD", solver_name="CBC")

        # binary  variable x_{ijk}
        x = [[[model.add_var(var_type=BINARY) for k in K] for j in N] for i in N]

        # binary variable y_{i}
        y = [model.add_var(var_type=BINARY, name=f"facility[{i}]") for i in I]

        # variable v_i
        v = [model.add_var(name=f"vehicles[{i}]") for i in I]

        # variable u_{jk}
        u = [[model.add_var() for k in K] for j in J]

        # variable z_s
        z = [model.add_var(var_type=BINARY) for s in S]

        # objective function (2.60)
        model.objective = minimize(
            xsum(distance_matrix[i][j] * x[i][j][k] for (i, j, k) in product(N, N, K))
            + xsum(fix_costs_facilities[i] * y[i] for i in I)
            + xsum(fix_costs_vehicles * v[i] for i in I)
        )

        # constraint (2.61)
        for j in J:
            model += xsum(x[i][j][k] for (i, k) in product(N, K)) == 1

        # # # constraint (2.62)
        for (j, k) in product(N, K):
            model += xsum(x[i][j][k] for i in N) - xsum(x[j][i][k] for i in N) == 0

        # # # constraint (2.63)
        for k in K:
            model += xsum(x[i][j][k] for (i, j) in product(I, J)) <= 1

        # # # constraint (2.64)
        for (j1, j2, k) in product(J, J, K):
            model += (
                u[j1 - number_of_facilities][k]
                - u[j2 - number_of_facilities][k]
                + number_of_customers * x[j1][j2][k]
                <= number_of_customers - 1
            )

        for i in I:
            model += xsum(x[i][j][k] for (j, k) in product(J, K)) - v[i] == 0

        for i in I:
            model += v[i] <= number_of_vehicles * y[i]

        # # # constraint (2.65)
        for (k, s) in product(K, S):
            model += (
                xsum(customer_demand[j - number_of_facilities][s] * x[i][j][k] for (j, i) in product(J, N))
                - vehicle_capacity
                - 100 * (1 - z[s])
                <= 0
            )

        # #prob
        model += xsum(probabilities[s] * z[s] for s in S) >= alpha

        # # # contraint (2.67)
        for (j, k) in product(J, K):
            model += u[j - number_of_facilities][k] >= 0

        return model

    def compute(self) -> None:
        self.model.optimize()

    def check_if_feasbile(self, solution_to_check: Union[str, bytes]) -> bool:
        pass
