from typing import Any, List, Union

from mip import BINARY, Model, minimize, xsum
import numpy as np
from pydantic import BaseModel

from master_thesis.models.base import Models


class UFLPDInputData(BaseModel):
    number_of_facilities: int
    number_of_customers: int
    distance_matrix: List[List[float]]
    fix_costs: List[float]


class UFLPD(Models):
    def __init__(self, input_data: Union[str, bytes]) -> None:
        self.data = UFLPDInputData.parse_raw(input_data)
        self.model = self.define_model()

    def define_model(self) -> Any:

        number_of_facilities = self.data.number_of_facilities
        number_of_customers = self.data.number_of_customers
        distance_matrix = np.array(self.data.distance_matrix)
        fix_costs = np.array(self.data.fix_costs)

        # set of indecies for facilities
        I = set(range(number_of_facilities))

        # set of indecies for customers
        J = set(range(number_of_customers))

        model = Model(name="UFLRD", solver_name="CBC")

        # continuous  variable x_{ij}
        x = [[model.add_var(var_type=BINARY, name=f"routes[{i}][{j}]") for j in J] for i in I]

        # binary variable y_{i}
        y = [model.add_var(var_type=BINARY, name=f"facility[{i}]") for i in I]

        # objective function (2.3)
        model.objective = minimize(
            xsum(distance_matrix[i][j + number_of_facilities] * x[i][j] for i in I for j in J)
            + xsum(fix_costs[i] * y[i] for i in I)
        )

        # constraint (2.4)
        for j in J:
            model += xsum(x[i][j] for i in I) == 1

        # constraint (2.5)
        for i in I:
            for j in J:
                model += x[i][j] <= y[i]

        return model

    def compute(self) -> None:
        self.model.optimize()

    def check_if_feasbile(self, solution_to_check: Union[str, bytes]) -> bool:
        pass
