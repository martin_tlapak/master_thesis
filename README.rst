************************
Location-routing problem
************************



Build docker image
******************

.. code-block:: bash

    docker build -t master-thesis-tlapak .

Run
***

.. code-block:: bash

    docker run -it -p 8888:8888 master-thesis-tlapak:latest


Run models
**************
Models can be run from notebooks folder.
