FROM python:3.8
WORKDIR /app

RUN python -m pip install poetry && poetry --version

COPY pyproject.toml poetry.lock ./
# RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

COPY master_thesis ./master_thesis
COPY notebooks ./notebooks
COPY data ./data

CMD poetry run jupyter notebook \
    --allow-root\
    --ip 0.0.0.0\
    --no-browser
